

'''
	https://github.com/astral-sh/uv
'''

import os
import time

def run (strand):
	print ("running:", strand)

	os.system (strand)
	
#
#	uv
#		curl -LsSf https://astral.sh/uv/install.sh | sh
#		source $HOME/.cargo/env
#
#			# or pip install uv
#	
#	uv venv:
#		uv venv
#		source .venv/bin/activate
#		uv pip sync requirements.txt 
#
#	system:
#		UV_SYSTEM_PYTHON=/habitat/venues/stages_pip2 uv pip sync requirements.txt --system
#
def build_with_uv ():
	def generate_uv_requirements ():
		run ("cd /habitat && rm requirements.txt")
		run ("cd /habitat && uv pip compile pyproject.toml -o requirements.txt")
		time.sleep (1)

	def build_uv_venv ():
		run ("cd /habitat && uv venv")
		run ("cd /habitat && . .venv/bin/activate")
		run ("cd /habitat && uv pip sync requirements.txt")

	run ("cd /habitat && rm -rf .venv")
	run ("pip install uv")
	
	generate_uv_requirements ()
	build_uv_venv ()

build_with_uv ()

