


print ("sync")

'''
def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'modules_pip'
])
'''


def rel_path (directory):
	import pathlib
	this_directory = pathlib.Path (__file__).parent.resolve ()

	from os.path import dirname, join, normpath
	import sys
	return normpath (join (this_directory, directory))

from_path = rel_path ("apps/web/dist")
to_path = rel_path ("../stages/trysts/adventures/sanique/public")

#print ("to_path:", to_path)
#exit ()

try:
	import shutil
	shutil.rmtree (to_path)
except:
	print ("couldn't remove to_path")

import pathlib
pathlib.Path (to_path).mkdir (parents=True, exist_ok=True)

import ships.paths.directory.rsync as rsync
rsync.process ({
	"from": from_path,
	"to": to_path,

	"start": "yes",
	
	#
	#	not implemented: "yes"
	#
	"ssh": "no",
	
	"sense": "yes"
})